# Sample Rest Server

This is a sample RESTful web server written in Node.js. It has sample HTTP GET, PUT, POST, and DELETE services. Resources are saved as JSON files (e.g., 0.json). To reset the record counter to 0, just delete counter.json. This is an example of a RESTful https server.

## Getting Started

1. Clone this repository.
1. Download and install [Node.js](https://nodejs.org/en/)
1. Install the required Node.js modules: go into the rest-server-https directory and run `npm install`
1. The server defaults to port 1234. Edit config.json to change the port number.

### Running the Server

```
$ node app.js
server running: https://localhost:1234/  (Ctrl+C to Quit)
```

### Testing the server from the command line

#### Add a record

```
user@yourmachine:~$  curl -k -i -H "Content-Type: application/json" -X POST -d '{"subject": "television","message": "i love television","channel":9}' https://localhost:1234/api/v1/addrecord
```

#### Update a record

```
user@yourmachine:~$  curl -k -i -H "Content-Type: application/json" -X PUT -d '{"subject": "new television","message": "new i love television","channel":99}' https://localhost:1234/api/v1/records/1.json
```

#### Get a record

```
user@yourmachine:~$  curl -k -i --request GET https://localhost:1234/api/v1/records/1.json
```

#### Delete a record

```
user@yourmachine:~$  curl -k -i --request DELETE https://localhost:1234/api/v1/records/1.json
```
